require('dotenv').config();
const ethers = require('ethers');
const contract = require("../artifacts/contracts/MyNFT.sol/MyNFT.json");

// Get Alchemy API Key
const API_KEY = process.env.API_KEY;

// Define an Alchemy Provider
const provider = new ethers.AlchemyProvider('sepolia', API_KEY);

console.log(JSON.stringify(contract.abi));